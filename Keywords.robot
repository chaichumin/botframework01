*** Keywords ***
SquareArea
    [Arguments]  ${paraWidth}  ${paramHeight}
    ${respArea}=  Evaluate  ${paraWidth} * ${paramHeight}
    [Return]  ${respArea}

